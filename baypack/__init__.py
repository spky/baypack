try:
    import lxml
    BS4_PARSER = 'lxml'
except ImportError:
    BS4_PARSER = 'html.parser'

FILE_ENCODING = 'utf-8'

CLASS_WELCOME = 'welcome-msg'
FORM_ID_LOGIN = 'loginFormClassicDobZip'
SELECT_POINTS = 'table.mypoints tbody tr'

URL_BASE = 'https://www.payback.de'
URL_LOGIN = '/pb/authenticate/id/713416'
URL_POINT = '/pb/punktekonto/pgn/{}/id/13598'

USER_AGENT = 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)'

from baypack.lib.args import arguments
from baypack.lib.log import bp_logger
from baypack.lib.conf import BayPackConfig, StorageConfig, DigestConfig
from baypack.baypack import BayPack
from baypack.storage import Storage
from baypack.digest import Digest
from baypack.out.ancient import Ancient
from baypack.out.feed import Feed
from baypack.out.page import Page
from baypack.out.plain import Plain
