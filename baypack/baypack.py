from itertools import repeat
from string import digits

from bs4 import BeautifulSoup
from requests import Session

from baypack import (
    BS4_PARSER, CLASS_WELCOME, FORM_ID_LOGIN, SELECT_POINTS, URL_BASE,
    URL_LOGIN, URL_POINT, USER_AGENT
)
from baypack.lib.clock import parse_date
from baypack.lib.log import LogBase


class BayPack(LogBase):
    def __init__(self, config):
        super().__init__(logger=config._logger)
        self._config = config
        self._session = Session()

        self.logged_in = False
        self.points = None

    def _date(self, dp):
        dp = self._zero(dp)
        if dp:
            return parse_date(dp)

    def _points(self, sp):
        return int(''.join([
            n for n in self._zero(sp, fb='0') if n in (digits + '-')
        ]))

    def _url(self, ep):
        return ''.join([URL_BASE, ep])

    def _zero(self, zp, fb=None):
        return zp.strip() if zp else fb

    @classmethod
    def dialup(cls, *args, **kwargs):
        bp = cls(*args, **kwargs)
        bp._login()
        return bp

    def _login(self):
        if self.logged_in:
            return self.l('already logged in')

        resp = self._session.get(self._url(URL_LOGIN))
        if resp.status_code != 200:
            return self.l('wrong login page response', resp.status_code)

        soup = BeautifulSoup(resp.content, BS4_PARSER)
        form = soup.find('form', id=FORM_ID_LOGIN)
        if not form:
            return self.l('login form not found on page')

        data = self._config()
        data.update(dict((
            n, form.select_one('[name={}]'.format(n))['value']
        ) for n in [
            'cid', 'did', 'language', 'page_id', 'secToken', 'token',
            '__checkbox_usePermLogin'
        ]))

        self.l('attempt login', data['cid'], data['did'], lvl='info')
        resp = self._session.post(form['action'], data=data, headers=dict([
            ('User-Agent', USER_AGENT),
        ]))
        if resp.status_code != 200:
            return self.l('login failed', 'sorry', resp.status_code)

        soup = BeautifulSoup(resp.content, BS4_PARSER)
        welc = soup.find('p', class_=CLASS_WELCOME)
        if not welc:
            return self.l('welcome message not found on page')

        self.points = self._points(welc.a.string)

        self.l('welcome', self._zero(welc.strong.string),
               'you have', self.points, 'points', lvl='info')
        self.logged_in = True

    def page(self, num=1):
        if num < 1:
            return self.l('this', num, 'is not possible', 'sorry')

        if not self.logged_in:
            return self.l('not logged in')

        self.l('processing account page', num, lvl='info')

        resp = self._session.get(self._url(URL_POINT.format(num)))
        if resp.status_code != 200:
            return self.l('wrong account page response', resp.status_code)

        soup = BeautifulSoup(resp.content, BS4_PARSER)
        for tr in soup.select(SELECT_POINTS):
            (date, dtc), (partner, pac), (action, acc), (points, ptc) = [(
                self._zero(d.string, fb=''), set(d.attrs.get('class', []))
            ) for d in tr.select('td')]

            yield dict(
                action=action,
                date=self._date(date),
                date_plain=date,
                kind=' '.join(sorted(dtc | pac | acc | ptc)),
                partner=partner,
                points=self._points(points),
            )

    def __call__(self, num=1):
        if not self.logged_in:
            return self.l('not logged in')

        for one in repeat(1):
            canary = False
            for row in self.page(num):
                yield row
                canary = True
            if canary:
                num += one
            else:
                self.l('you\'ve reached the end', num, lvl='warning')
                break
        return self
