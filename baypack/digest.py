from baypack.lib.log import LogBase


class Digest(LogBase):
    def __init__(self, storage, config):
        super().__init__(logger=config._logger)
        self._config = config
        self._storage = storage
        self._chain = []

        self._chain_kwargs = dict(
            points=self._storage._baypack.points,
            now=self._storage._now,
            recent=self._storage._recent
        )

    def add(self, *handlers):
        [self._chain.append(
            h(self._config, **self._chain_kwargs)
        ) for h in handlers]
        self.l('added', ', '.join([h.__name__ for h in handlers]), lvl='info')
        return self

    def __call__(self):
        self.l('running', self.__class__.__name__, 'chain with', ', '.join([
            h.__class__.__name__ for h in self._chain
        ]), lvl='info')

        [link(
            items, full, self._storage._config.base_dir
        ) for items, full in [(
            self._storage._query.limit(self._config.entry_limit).all(), False
        ), (
            self._storage._query.all(), True
        )] for link in self._chain]
        return self
