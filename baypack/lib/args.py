from argparse import ArgumentParser


def arguments():
    parser = ArgumentParser('BayPack')
    parser.add_argument(
        '--baypack', help='baypack.json config',
        default='baypack.json'
    )
    parser.add_argument(
        '--storage', help='storage.json config',
        default='storage.json'
    )
    parser.add_argument(
        '--digest', help='digest.json config',
        default='digest.json'
    )
    parser.add_argument(
        '--start', help='start at page',
        type=int, default=1
    )
    parser.add_argument(
        '--empty', help='stop after .. empty pages',
        type=int, default=5
    )
    return parser.parse_args()
