from datetime import datetime

from dateutil import parser


def timestamp():
    return datetime.utcnow()


def parse_date(ds):
    return parser.parse(ds, dayfirst=True)
