from baypack.lib.disk import read_json, write_json
from baypack.lib.log import LogBase


class ConfBase(LogBase):
    def __init__(self, logger=None):
        super().__init__(logger=logger)

    @classmethod
    def load(cls, location, logger=None):
        cf = cls(logger=logger, **read_json(
            location, fallback={}, logger=logger
        ))
        cf.l('loaded', cls.__name__, 'config from', location, lvl='info')
        return cf

    def dump(self, location):
        self.l('dumping', self.__class__.__name__, 'config to', location,
               lvl='info')
        return write_json(location, dict(
            (a, v) for a, v in vars(self).items() if not a.startswith('_')
        ), logger=self._logger)


class BayPackConfig(ConfBase):
    def __init__(self, logger=None, **kwargs):
        super().__init__(logger=logger)
        self.num = kwargs.get('num', 0)
        self.plz = kwargs.get('plz', 0)
        self.yy = kwargs.get('yy', 0)
        self.mm = kwargs.get('mm', 0)
        self.dd = kwargs.get('dd', 0)

    def __call__(self):
        return {
            'model.cardNumber': '{:010d}'.format(self.num),
            'zipCode': '{:05d}'.format(self.plz),
            'dobDay': '{:02d}'.format(self.dd),
            'dobMonth': '{:02d}'.format(self.mm),
            'dobYear': '{:02d}'.format(self.yy)
        }


class StorageConfig(ConfBase):
    def __init__(self, logger=None, **kwargs):
        super().__init__(logger=logger)
        self.base_dir = kwargs.get('base_dir', '_')
        self.store = kwargs.get('store', 'infinite.db')
        self.today = kwargs.get('today', 'today.json')

    def __call__(self):
        return 'sqlite:///{}'.format(self.store if self.store else ':memory:')


class DigestConfig(ConfBase):
    def __init__(self, logger=None, **kwargs):
        super().__init__(logger=logger)
        self.ancient_filename = kwargs.get(
            'ancient_filename', 'export.csv'
        )
        self.ancient_full_filename = kwargs.get(
            'ancient_full_filename', 'export_full.csv'
        )
        self.base_url = kwargs.get(
            'base_url', 'http://www.example.com'
        )
        self.entry_limit = kwargs.get(
            'entry_limit', 42
        )
        self.feed_author = kwargs.get(
            'feed_author', 'Point Collectors'
        )
        self.feed_filename = kwargs.get(
            'feed_filename', 'feed.atom'
        )
        self.feed_full_filename = kwargs.get(
            'feed_full_filename', 'feed_full.atom'
        )
        self.feed_full_title = kwargs.get(
            'feed_full_title', 'BayPack Full Feed'
        )
        self.feed_subtitle = kwargs.get(
            'feed_subtitle', 'Collecting Points'
        )
        self.feed_title = kwargs.get(
            'feed_title', 'BayPack Feed'
        )
        self.page_filename = kwargs.get(
            'page_filename', 'index.html'
        )
        self.page_full_filename = kwargs.get(
            'page_full_filename', 'index_full.html'
        )
        self.page_full_title = kwargs.get(
            'page_full_title', 'BayPack Full Page'
        )
        self.page_title = kwargs.get(
            'page_title', 'BayPack Page'
        )
        self.plain_filename = kwargs.get(
            'plain_filename', 'export.json'
        )
        self.plain_full_filename = kwargs.get(
            'plain_full_filename', 'export_full.json'
        )

    def __call__(self, *elems, delimiter='/'):
        if all(elems):
            return delimiter.join(e for e in elems)
