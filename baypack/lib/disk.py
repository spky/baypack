from codecs import open as c_open
from json import dumps, loads
from os import path

from baypack import FILE_ENCODING
from baypack.lib.log import _log


def read_file(location, fallback=None, logger=None):
    if location and path.isfile(location):
        with c_open(location, 'r', FILE_ENCODING) as f:
            dt = f.read()
            if dt is not None:
                return dt
    _log('error reading file', 'fallback', fallback, lvl='warning',
         logger=logger)
    return fallback


def read_json(location, fallback={}, logger=None):
    data = read_file(location, logger=logger)
    if data:
        try:
            return loads(data)
        except ValueError:
            _log('error unpickling json', 'fallback', fallback, lvl='warning',
                 logger=logger)
    return fallback


def write_file(location, data, logger=None):
    if location:
        with c_open(location, 'w', FILE_ENCODING) as f:
            f.write(data)
            return True
            _log('writen file', location, lvl='info', logger=logger)
    _log('error writing file', location, lvl='error', logger=logger)
    return False


def write_json(location, content, logger=None):
    try:
        return write_file(location, dumps(
            content, indent=2, sort_keys=True
        ), logger=logger)
    except TypeError as ex:
        _log('error pickling json', ex, lvl='error', logger=logger)
    return False
