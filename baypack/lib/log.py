from logging import DEBUG, Formatter, StreamHandler, getLogger

bp_logger = getLogger('baypack')
logfmt = Formatter('\t%(levelname)s\t[%(asctime)s]\n%(message)s')

stderr = StreamHandler(stream=None)
stderr.setFormatter(logfmt)

bp_logger.addHandler(stderr)
bp_logger.setLevel(DEBUG)


def _log(*msgs, lvl=None, logger=None):
    '''to disable set logger to ``False``'''
    log = bp_logger if logger is None else logger
    if log:
        dict(
            info=log.info, warning=log.warning
        ).get(lvl, log.error)(
            ' | '.join([str(m) for m in msgs])
        )


class LogBase:
    def __init__(self, logger=None):
        self._logger = logger

    def l(self, *msgs, lvl=None):
        _log(*msgs, lvl=lvl, logger=self._logger)
