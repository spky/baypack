from sqlalchemy import Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import backref, relationship

Base = declarative_base()


class BaseMixin:
    id = Column(Integer, primary_key=True)

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    def __repr__(cls):
        return '<{} #{} {}>'.format(
            cls.__tablename__, cls.id, getattr(cls, 'val', '')
        )


class Action(BaseMixin, Base):
    val = Column(String)


class Date(BaseMixin, Base):
    val = Column(DateTime)


class Kind(BaseMixin, Base):
    val = Column(String)


class Partner(BaseMixin, Base):
    val = Column(String)


class Point(BaseMixin, Base):
    val = Column(Integer)


class Infinite(BaseMixin, Base):
    action = relationship('Action', backref=backref('inf', lazy='dynamic'))
    action_id = Column(Integer, ForeignKey('action.id'))

    date = relationship('Date', backref=backref('inf', lazy='dynamic'))
    date_id = Column(Integer, ForeignKey('date.id'))

    kind = relationship('Kind', backref=backref('inf', lazy='dynamic'))
    kind_id = Column(Integer, ForeignKey('kind.id'))

    partner = relationship('Partner', backref=backref('inf', lazy='dynamic'))
    partner_id = Column(Integer, ForeignKey('partner.id'))

    point = relationship('Point', backref=backref('inf', lazy='dynamic'))
    point_id = Column(Integer, ForeignKey('point.id'))

    def flat(self, with_id=False):
        return dict(
            action=self.action.val,
            date=self.date.val.isoformat(),
            kind=self.kind.val,
            partner=self.partner.val,
            point=self.point.val,
            **dict(id=self.id) if with_id else dict()
        )
