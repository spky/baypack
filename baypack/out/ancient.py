from csv import DictWriter

from baypack.lib.log import LogBase


class Ancient(LogBase):
    def __init__(self, config, **kwargs):
        super().__init__(logger=config._logger)
        self._config = config

    def __call__(self, items, full, base_dir):
        filename = self._config(base_dir, (
            self._config.ancient_full_filename if full else
            self._config.ancient_filename
        ))

        if filename:
            self.l('generating ancient', filename, lvl='info')
            _first = items[0].flat() if items else dict()
            with open(
                    filename, 'w', newline='', encoding='utf-8'
            ) as csv_handler:
                writer = DictWriter(
                    csv_handler, fieldnames=sorted(_first.keys()),
                    restval='?!?', extrasaction='ignore',
                )
                writer.writeheader()
                [writer.writerow(i.flat()) for i in items]
