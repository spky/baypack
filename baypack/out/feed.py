from werkzeug.contrib.atom import AtomFeed, FeedEntry

from baypack.lib.disk import write_file
from baypack.lib.log import LogBase


class Feed(LogBase):
    def __init__(self, config, **kwargs):
        super().__init__(logger=config._logger)
        self._config = config

    def feed(self, *, title, subtitle, author, url, feed_url, items):
        return AtomFeed(
            title=title, subtitle=subtitle,
            url=url, feed_url=feed_url,
            author=author,
            generator=('BayPack', url, '10FACH °P'),
            entries=[FeedEntry(
                title='{} | {}'.format(i.action.val, i.point.val),
                summary='{}'.format(i.point.val),
                content='{} | {} | {}'.format(
                    i.action.val, i.point.val,
                    i.partner.val
                ),
                published=i.date.val, updated=i.date.val,
                url='{}/#{}'.format(url, i.id),
                categories=[
                    dict(term='action', label=i.action.val),
                    dict(term='kind', label=i.kind.val),
                    dict(term='partner', label=i.partner.val),
                    dict(term='point', label=i.point.val),
                ]
            ) for i in items]
        )

    def __call__(self, items, full, base_dir):
        filename = self._config(base_dir, (
            self._config.feed_full_filename if full else
            self._config.feed_filename
        ))

        if filename:
            self.l('generating feed', filename, lvl='info')
            write_file(filename, str(self.feed(
                title=(
                    self._config.feed_full_title if full else
                    self._config.feed_title
                ),
                subtitle=self._config.feed_subtitle,
                author=self._config.feed_author,
                url=self._config.base_url,
                feed_url=self._config(self._config.base_url, filename),
                items=items
            )), logger=self._logger)
