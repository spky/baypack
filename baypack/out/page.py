from json import dumps
from os import path

from jinja2 import Environment, FileSystemLoader

from baypack.lib.disk import write_file
from baypack.lib.log import LogBase


class Page(LogBase):
    def __init__(self, config, **kwargs):
        super().__init__(logger=config._logger)
        self._config = config
        self._now = kwargs.get('now', '?!?')
        points = kwargs.get('points')
        self._points = '{} °P'.format(points) if points else ''

        environment = Environment(
            loader=FileSystemLoader(path.dirname(__file__))
        )
        self._template = environment.get_template('page.html')

    def __call__(self, items, full, base_dir):
        filename = self._config(base_dir, (
            self._config.page_full_filename if full else
            self._config.page_filename
        ))

        if filename:
            self.l('generating page', filename, lvl='info')
            write_file(
                filename,
                self._template.render(
                    created=self._now,
                    feed_url=self._config(self._config.base_url, (
                        self._config.feed_full_filename if full else
                        self._config.feed_filename
                    )),
                    feed_title=(
                        self._config.feed_full_title if full else
                        self._config.feed_title
                    ),
                    payload=dumps([i.flat(with_id=True) for i in items]),
                    points=self._points,
                    title=(
                        self._config.page_full_title if full else
                        self._config.page_title
                    ),
                ),
                logger=self._logger
            )
