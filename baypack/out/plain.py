from baypack.lib.disk import write_json
from baypack.lib.log import LogBase


class Plain(LogBase):
    def __init__(self, config, **kwargs):
        super().__init__(logger=config._logger)
        self._config = config

    def __call__(self, items, full, base_dir):
        filename = self._config(base_dir, (
            self._config.plain_full_filename if full else
            self._config.plain_filename
        ))

        if filename:
            self.l('generating plain', filename, lvl='info')
            write_json(
                filename, [i.flat() for i in items], logger=self._logger
            )
