from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from baypack.lib.clock import timestamp
from baypack.lib.disk import read_json, write_json
from baypack.lib.log import LogBase
from baypack.lib.models import (
    Action, Base, Date, Infinite, Kind, Partner, Point
)


class Storage(LogBase):
    def __init__(self, baypack, config):
        super().__init__(logger=config._logger)
        self._baypack = baypack
        self._config = config

        engine = create_engine(self._config())
        Base.metadata.create_all(engine)
        db_session = sessionmaker(bind=engine)

        self._session = db_session()
        self._recent = []
        self._now = timestamp()

    @property
    def _query(self):
        return self._session.query(
            Infinite
        ).join(Infinite.date).order_by(Date.val.desc())

    def _sel(self, S, dt, _create=False):
        if not isinstance(dt, dict):
            dt = dict(val=dt)
        elem = self._session.query(S).filter_by(**dt).first()
        if not elem and _create:
            self.l('adding new', S.__tablename__, ' '.join(
                '{}=\'{}\''.format(k, v) for k, v in sorted(dt.items())
            ), lvl='info')
            elem = S(**dt)
            self._session.add(elem)
        return elem

    def _inf(self, dt, _create=False):
        return self._sel(Infinite, dict(
            action=self._sel(Action, dt.get('action'), _create=_create),
            date=self._sel(Date, dt.get('date'), _create=_create),
            kind=self._sel(Kind, dt.get('kind'), _create=_create),
            partner=self._sel(Partner, dt.get('partner'), _create=_create),
            point=self._sel(Point, dt.get('points'), _create=_create),
        ), _create=_create)

    def _store_batch(self, items):
        [self._inf(dt, _create=True) for dt in items]
        self._session.commit()
        self.l('stored', len(items), 'items', lvl='info')
        return True

    def _day_batch(self, items, day):
        length = len(items)
        if not length:
            return self.l(day, 'skipping empty batch', lvl='info')

        date = self._sel(Date, day, _create=False)
        if not date:
            self.l(day, 'storing new batch', length, lvl='info')
            return self._store_batch(items)
        size = date.inf.count()
        if size > length:
            return self.l(day, 'storage', size, '>', length, 'source', 'bug?')
        if size == length:
            return self.l(day, 'batch fully present', size, lvl='info')

        lost = [i for i in items if not self._inf(i, _create=False)]
        if lost:
            self.l(day, 'recovering', len(lost), 'items', lvl='warn')
            return self._store_batch(lost)

    def _today_batch(self, items, day):
        today_file = '/'.join([self._config.base_dir, self._config.today])
        today = read_json(today_file, fallback={}, logger=self._logger)
        cur_len = len(today.get('elems', []))
        if cur_len == len(items):
            return self.l('skipping today store', 'no new entries', lvl='info')

        today['elems'] = [dict(date=i.get('date_plain'), **dict(
            (k, v) for k, v in i.items() if k not in ['date', 'date_plain']
        )) for i in items]
        today['last_edit'] = self._now.isoformat()

        write_json(today_file, today, logger=self._logger)
        self._recent = today['elems'][cur_len:]

        self.l('stored', len(items), 'for today', lvl='info')

    def __call__(self, start=1, allow_empty=5):
        iterator = self._baypack(start)
        first = next(iterator)
        today_batch, batch = [first], []
        today = first.get('date')
        last_day, empty = today, 0

        for item in iterator:
            day = item.get('date')
            if today == day:
                today_batch.append(item)
            else:
                if today_batch:
                    self._today_batch(today_batch, last_day)
                    today_batch = []
                if allow_empty and allow_empty <= empty:
                    self.l('stopping storage, too many', allow_empty,
                           'empty pages', lvl='warn')
                    break
                if last_day != day:
                    empty += (0 if self._day_batch(batch, last_day) else 1)
                    batch = []
                batch.append(item)
            last_day = day
        else:
            if today_batch:
                self._today_batch(today_batch, last_day)
            self._day_batch(batch, last_day)

        self.l('storage done', allow_empty if allow_empty else 'all',
               'pages stored', lvl='info')
        return self
