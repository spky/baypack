#!/usr/bin/env python3

from baypack import (
    Ancient, BayPack, BayPackConfig, Digest, DigestConfig, Feed, Page, Plain,
    Storage, StorageConfig, arguments
)


def run():
    args = arguments()
    Digest(
        Storage(
            BayPack.dialup(BayPackConfig.load(args.baypack)),
            StorageConfig.load(args.storage)
        )(start=args.start, allow_empty=args.empty),
        DigestConfig.load(args.digest)
    ).add(Ancient, Feed, Page, Plain)()


if __name__ == '__main__':
    run()
